package com.company;

import java.util.ArrayList;
import java.util.List;

public class Wrapper<T> {
    List<T> myList = new ArrayList<>();
    int count = 0;

    void addItem(T t) throws DuplicateException {
        for (int i = 0; i < myList.size(); i++) {
            if (t == getItem(i)){
                count ++;
            }
        }
        if (t == null){
            throw new NullPointerException("Null value");
        }else if( count != 0){
            throw new DuplicateException(" duplicate value");
        }else{
            myList.add(t);
        }
    }
    T getItem ( int i){
        return myList.get(i);
    }
}
